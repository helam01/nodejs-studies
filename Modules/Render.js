var fs   = require('fs');
var ejs = require('ejs');

exports.Render = function(req, res)
{
    this.req;
    this.res;

    this.constructor = function(req, res)
    {
        this.req = req;
        this.res = res;
    }


    this.renderView = function(view, data)
    {
        let self = this;
        this.res.writeHead(200, {'Content-Type':'text/html'});

        let layout = "./views/layout/default.html";

        view = view+".ejs";

        let options = {};

        data = (data !== undefined) ? data : {};

        data.content = view;
        ejs.renderFile(layout, data, options, function(err, str){
            if (err) {
                self.res.write('Página não encontrada');
            } else {
                self.res.write(str);
            }

        });

        return self.res.end();

    }
    
    this.constructor(req, res);

    return this;
}