exports.routes = function()
{
    return [
        {"path" : "", "controller":"HomeController", "method":"index"},
        {"path" : "/", "controller":"HomeController", "method":"index"},
        
        {"path" : "/contato", "controller":"ContatoController", "method":"index"},
        {"path" : "/contato/enviar", "controller":"ContatoController", "method":"enviar"},

        {"path" : "/login", "controller":"AuthController", "method":"index"},
        {"path" : "/login/auth", "controller":"AuthController", "method":"auth"}
    ];
}
