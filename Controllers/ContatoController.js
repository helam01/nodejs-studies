var moduleRender = require('../Modules/Render');

exports.ContatoController = function(req, res)
{
    this.req;
    this.res;

    this.constructor = function(req, res)
    {
        this.req = req;
        this.res = res;

        this.render = moduleRender.Render(req,res);
    }


    this.index = function()
    {
        console.log('ContatoController Index');
        let data = {user:{name:"Helam"}};
        this.render.renderView('contato/index', data);
    }

    this.enviar = function()
    {
        console.log('ContatoController Enviar');
        this.res.writeHead(302, {'Location': '/contato?act=sent'});
        this.res.end();
    }

    this.constructor(req, res);
    
    return this;
}