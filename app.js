var http = require('http');
var url  = require('url');
var moduleRouter = require('./Modules/Router');

http.createServer(function(req,res){
    console.log('::Request start:: ==========================');
    console.log(req.url);

    let router = moduleRouter.Router(req, res);
    
    let queryStrings = url.parse(req.url, true).query;
    console.log(queryStrings);
    /*
    let response = 'NodeJS App <br />';
        response += 'Route : '+req.url+' <br />';
        response += 'a : '+queryStrings.a+' ::';
        response += 'b : '+queryStrings.b+' <br />';

    res.end(response);
    */
}).listen(8000);