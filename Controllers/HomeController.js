var moduleRender = require('../Modules/Render');

exports.HomeController = function(req, res)
{
    this.req;
    this.res;

    this.constructor = function(req, res)
    {
        this.req = req;
        this.res = res;

        this.render = moduleRender.Render(req,res);
    }


    this.index = function()
    {
        console.log('HomeController Index');
        let data = {user:{name:"Helam"}};
        this.render.renderView('home/index', data);
    }

    this.constructor(req, res);
    
    return this;
}