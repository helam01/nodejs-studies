var url     = require('url');
var routes  = require('../config/routes');
var routeFS = require('fs');

exports.Router = function(req, res)
{
    this.routes = routes.routes();
    this.req;
    this.res;

    this.constructor = function(req, res)
    {
        this.req = req;
        this.res = res;

        this.parseRoute(req);
    }

    this.parseRoute = function(req)
    {
        let url_parts = req.url.split('?');

        this.dispacheRoute(url_parts[0]);
    }

    this.dispacheRoute = function(routePath)
    {
        let self = this;

        let route = false;

        for(let i in this.routes) {
            if(this.routes[i].path == routePath) {
                route = this.routes[i];
            }
        }

        if ( route ) {
            let moduleController = require('../Controllers/'+route.controller);
            let controller = moduleController[route.controller](this.req, this.res);
            try {
                controller[route.method]();
                
            } catch (error) {
                return self.pageNotFound();
            }
        } else {

            if ( !routeFS.existsSync('.'+this.req.url) ) {
                return self.pageNotFound();   
            }

            return this.res.end();
        }

    }

    this.pageNotFound = function()
    {
        this.res.writeHead(403, {'Content-Type':'text/html'});
        return this.res.end('Página n encontrada');
    }

    this.constructor(req, res);
    return this;

}
