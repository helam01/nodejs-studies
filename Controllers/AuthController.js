var url  = require('url');
var formidable = require('formidable');

var moduleRender = require('../Modules/Render');

var AuthService = require('../Services/AuthService');

exports.AuthController = function(req, res)
{
    this.req;
    this.res;

    this.constructor = function(req, res)
    {
        this.req = req;
        this.res = res;

        this.render = moduleRender.Render(req,res);
        this.authService = AuthService.AuthService();
    }

    this.index = function()
    {
        console.log('AuthController Index');
        this.render.renderView('auth/index');
    }

    this.auth = function()
    {
        let self = this;
        let queryStrings = url.parse(this.req.url, true).query;

        let form = new formidable.IncomingForm();
        form.parse(this.req, function(err, fields, files){
            console.log('AuthController Auth');
            
            let user = self.authService.checkAuth(fields);
            console.log(user);

            self.res.end();
        });
    }

    this.constructor(req, res);
    
    return this;
}